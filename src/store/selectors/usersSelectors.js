import { isEmpty, some } from 'lodash'

export const usersSelector = state => state.users

export const checkIfUserAlreadyExist = (users, {email}) => {
	if (!users || isEmpty(users)) {
		return false
	}
	return some(users, user => user.email === email)

}

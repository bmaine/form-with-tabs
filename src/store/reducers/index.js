import {combineReducers} from 'redux'
import usersReducer from './usersReducer'
import { reducer as formReducer } from 'redux-form'

export const rootReducer = combineReducers({
	form: formReducer,
	users: usersReducer
})

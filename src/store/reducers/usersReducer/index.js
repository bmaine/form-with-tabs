import produce from 'immer'
import {reduce} from 'lodash'
import {CREATE_NEW_USER, INIT_USER_STORE, LOGIN_USER, LOGOUT_USER} from '../../actions/users'
import {checkIfUserAlreadyExist} from '../../selectors/usersSelectors'

const initialUserState = {
	users: [{
		userName: 'Ben',
		email: 'benjamin.maine@gmail.com',
		firstName: 'benjamin',
		isConnected: false
	}]
}

const usersReducer = produce((draft , action) => {
	const {users} = draft
	const {payload} = action
	switch (action.type) {
	case INIT_USER_STORE:
		return initialUserState
	case LOGIN_USER:
		if(checkIfUserAlreadyExist(draft.users, payload)){
			throw new Error('you already have an acount with this email address')
		}
		draft.users = [...users, {...payload, isConnected: true}]
		break
	case LOGOUT_USER:
		draft.users = reduce(users, (result, user) => {
			if(user.email === payload) {
				user.isConnected = false
			}
			return [...result, user]
		}, [])
		break
	case CREATE_NEW_USER:
		if(checkIfUserAlreadyExist(draft.users, {...payload, isConnected: true})) {
			return draft
		}
		return draft
	default: return draft
	}

}, {})


export default  usersReducer
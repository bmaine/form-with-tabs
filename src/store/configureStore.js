import {applyMiddleware, createStore} from 'redux'
import {rootReducer} from './reducers'
import thunkMiddleware from 'redux-thunk'

const configureStore = () => {
	const reduxThunkMiddleWare = applyMiddleware( thunkMiddleware)
	const store = createStore(rootReducer, undefined, reduxThunkMiddleWare)
	return store
}

export default configureStore

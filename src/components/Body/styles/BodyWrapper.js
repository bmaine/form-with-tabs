import styled from 'styled-components'

const BodyWrapper = styled.div`
      min-height: 70vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      color: black;
`

export default BodyWrapper
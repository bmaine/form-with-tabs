import React from 'react'
import {BodyWrapper} from './styles'
import ContactForm from '../ContactForm'
import Confirmation from '../Confirmation'
import {useTabContext} from '../../designSystem/TabPanel/hooks'
import {StepSelected} from '../../constants'

const Body = () => {
	const {tabSelected} = useTabContext()

	return(
		<BodyWrapper>
			{tabSelected === StepSelected.GENERAL && <ContactForm/>}
			{tabSelected === StepSelected.CONFIRMATION && <Confirmation/>}
		</BodyWrapper>
	)

}


export default Body
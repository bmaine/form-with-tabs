import React from 'react'
import HeaderWrapper from './styles/HeaderWrapper'
import { TabPanel } from '../../designSystem'

const tabs = [
	{
		id: 0,
		key: 'basic-info',
		text: 'General',
	},
	{
		id: 1,
		key: 'final-confirmation',
		text: 'Confirmation',
		title: 'Confirmation',
	},
]

const Header = () =>
	<HeaderWrapper>

		<TabPanel.Title  title="Sign in">
		</TabPanel.Title>
		<TabPanel.Tab tabs={tabs}/>

	</HeaderWrapper>
	

export default Header
import styled from 'styled-components'

const HeaderWrapper = styled.div`
  margin-top: 2vh;
  height: 20vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: black;
`

export default HeaderWrapper
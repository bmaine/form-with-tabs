import React, {useMemo, useState} from 'react'
import { Field, reduxForm } from 'redux-form'
import {shallowEqual, useSelector} from 'react-redux'
import {  CircleLoader } from '../../designSystem'
import {formSelector} from '../../store/selectors/formSelectors'
import {checkAllFieldArePresent} from '../../helpers/checkAllFieldArePresent'
import {useTabContext} from '../../designSystem/TabPanel/hooks'
import {FieldWrapper, FormWrapper} from './styles'
import { useDispatch } from 'react-redux'
import {StepSelected} from '../../constants'

const ContactForm = () => {
	const formState = useSelector(formSelector, shallowEqual)
	const [isLoading, setIsLoading] = useState(false)
	const {setTabSelected} = useTabContext()
	const dispatch = useDispatch()

	const isFormCompleted = useMemo(() => {
		const formFields = formState?.contact?.values
		if(!formFields || !checkAllFieldArePresent(formFields)) {
			return false
		}
		return true
	}, [formState])

	const handleSubmit = (e) => {
		setIsLoading(true)
		e.preventDefault()
		e.stopPropagation()
		const {contact: {values : {firstName, email, userName}}} = formState
		setTimeout(() => {
			try {
				dispatch({type: 'LOGIN_USER', payload: {firstName, userName, email}})
				isFormCompleted && setTabSelected(StepSelected.CONFIRMATION)
			}
			catch (e) {
				alert(e)
			}
			setIsLoading(false)
		}, 2000)
	}

	return (
		<>
			{isLoading ? (
				<CircleLoader/>
			) : (
				<FormWrapper onSubmit={handleSubmit}>
					<FieldWrapper>
						<label style={{marginBottom: '2vh'}} htmlFor="userName">UserName</label>
						<Field name="userName" component="input" type="text" />
					</FieldWrapper>
					<FieldWrapper >
						<label style={{marginBottom: '2vh'}} htmlFor="firstName">First Name</label>
						<Field name="firstName" component="input" type="text" />
					</FieldWrapper>
					<FieldWrapper>
						<label style={{marginBottom: '2vh'}} htmlFor="email">Email</label>
						<Field name="email" component="input" type="email" />
					</FieldWrapper>
					<FieldWrapper>
						<label style={{marginBottom: '2vh'}} htmlFor="password">Password</label>
						<Field name="password" component="input" type="password" />
					</FieldWrapper>
					<button disabled={!isFormCompleted} type="submit">Submit</button>
				</FormWrapper>
			)}

		</>
	)
}

export default reduxForm({
	form: 'contact'
})(ContactForm)
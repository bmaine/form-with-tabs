import styled from 'styled-components'

const FormWrapper = styled.form`
      min-height: 70vh;
      min-width: 90vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      color: black;
`

export default FormWrapper
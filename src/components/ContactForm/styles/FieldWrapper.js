import styled from 'styled-components'

const FieldWrapper = styled.div`
      width: 50%;
      display: flex;
      flex-direction: column;
      text-align: left;
      margin-bottom: 2vh;
`

export default FieldWrapper


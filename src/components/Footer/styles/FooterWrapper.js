import styled from 'styled-components'

const FooterWrapper = styled.div`
      min-height: 10vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      color: black;
`

export default FooterWrapper
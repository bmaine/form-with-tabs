import React, {useMemo} from 'react'
import {find} from 'lodash'
import {shallowEqual, useSelector} from 'react-redux'
import {usersSelector} from '../../store/selectors/usersSelectors'
import {useTabContext} from '../../designSystem/TabPanel/hooks'
import { useDispatch } from 'react-redux'
import { LOGOUT_USER } from '../../store/actions/users'
import {StepSelected} from '../../constants'

const Confirmation = () => {
	const usersState = useSelector(usersSelector, shallowEqual)
	const {setTabSelected} = useTabContext()
	const dispatch = useDispatch()

	const userConnected = useMemo(() => {
		const {users} = usersState
		const userConnected = find(users, user => {
			return user.isConnected
		})
		return userConnected
	}, [usersState])

	const handleLogOut = () => {
		const {email} = userConnected
		dispatch({type: LOGOUT_USER, payload: email})
		setTabSelected(StepSelected.GENERAL)
	}

	return (
		<>
			<h1>Congrats you are in !</h1>
			<button onClick={handleLogOut}>Log out</button>
		</>
	)
}

export default Confirmation
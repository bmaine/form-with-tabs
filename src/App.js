import React, {useEffect} from 'react'
import {Body, Header} from './components'
import { useDispatch } from 'react-redux'
import {TabPanel} from './designSystem'


function App() {
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch({type: 'INIT_USER_STORE'})
	}, [dispatch])
	return (
		<div className="App">
			<TabPanel>
				<Header/>
				<Body/>
			</TabPanel>
		</div>
	)
}

export default App

import { expect } from 'chai'
import { checkIfUserAlreadyExist } from '../helpers/checkIfUserAlreadyExist'


describe('checkIfUserAlreadyExist', () => {
	it('should return false if state is empty', () => {
		const output = checkIfUserAlreadyExist([])
		expect(output).to.be.false
	})
	it('should return true if users is in the state', () => {
		const output = checkIfUserAlreadyExist(stateWithUsersInserted, userPresentInState)
		expect(output).to.be.true
	})

	it('should return false if users is not in the state', () => {
		const output = checkIfUserAlreadyExist(stateWithUsersInserted, userNotPresentInState)
		expect(output).to.be.false
	})
})

const stateWithUsersInserted = [
	{
		userName: 'Ben',
		email: 'benjamin.maine@gmail.com',
		password: 'password',
	},
	{
		userName: 'Ben1',
		email: 'benjamin.maine+1@gmail.com',
		password: 'password',
	},
	{
		userName: 'Ben2',
		email: 'benjamin.maine+2@gmail.com',
		password: 'password',
	}
]

const userPresentInState = {
	userName: 'Ben',
	email: 'benjamin.maine@gmail.com',
	password: 'password',
}

const userNotPresentInState = {
	userName: 'Ben',
	email: 'benjamin.maine+4@gmail.com',
	password: 'password',
}
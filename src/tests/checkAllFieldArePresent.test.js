import { expect } from 'chai'
import { checkAllFieldArePresent } from '../helpers/checkAllFieldArePresent'

describe('checkAllFieldArePresent', () => {
	it('should return false if user is empty', () => {
		const output = checkAllFieldArePresent({})
		expect(output).to.be.false
	})
	it('should return true if user has all asked property', () => {
		const output = checkAllFieldArePresent(correctUser)
		expect(output).to.be.true
	})

	it('should return false if user has no email', () => {
		const output = checkAllFieldArePresent(emptyEmailUser)
		expect(output).to.be.false
	})
})


const correctUser = {
	firstName: 'Ben',
	email: 'benjamin.maine@gmail.com',
	userName: 'Ben2',
}

const emptyEmailUser = {
	firstName: 'Ben',
	email: '',
	userName: 'password',
}

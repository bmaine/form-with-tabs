import React from 'react'
import {TabsProvider} from './hooks'
import TabsWrapper from './TabsWrapper'
import {TabsTitle} from './components'
import {TabsTab} from './components'


const TabPanel = ({tabSelected, ...rest}) => {
	return(
		<TabsProvider tabSelected={tabSelected}>
			<TabsWrapper{...rest}/>
		</TabsProvider>
	)
}

TabPanel.Title = TabsTitle
TabPanel.Tab = TabsTab

export default TabPanel
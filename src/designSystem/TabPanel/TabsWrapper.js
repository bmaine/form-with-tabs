import styled from 'styled-components'

const TabsWrapper = styled.div`
    flex-direction: column;
    min-width: 100vh;
`

export default TabsWrapper
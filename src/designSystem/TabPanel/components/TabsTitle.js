import React from 'react'
import PropsTypes from 'prop-types'

const Title = ({title}) => {
	return(
		<h3>{title}</h3>
	)
}

Title.propTypes = {
	title: PropsTypes.string
}

export default Title

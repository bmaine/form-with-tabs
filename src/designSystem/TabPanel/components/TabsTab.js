import React from 'react'
import PropsTypes from 'prop-types'
import styled from 'styled-components'
import {map} from 'lodash'
import {useTabContext} from '../hooks'

import {Tab} from '../../index'

const TabsTabWrapper = styled.div`
	display: flex;
	flex: 1;
	flex-direction: row;
	justify-content: center;
`

const TabsTab = ({tabs}) => {
	const {tabSelected} = useTabContext()
	return(
		<TabsTabWrapper>
			{map(tabs,
				({id, key, text}) => <Tab isSelected={tabSelected === id} key={key} text={text}>Tab1</Tab>
			)}
		</TabsTabWrapper>
	)
}

TabsTab.propTypes = {
	key: PropsTypes.string,
	text: PropsTypes.string
}

export default TabsTab

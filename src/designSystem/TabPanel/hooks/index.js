import React, { createContext, useContext, useState} from 'react'

const TabContext = createContext()

const StepSelected = {
	GENERAL: 0,
	CONFIRMATION: 1,
}

const useTabContext =() => {
	const context = useContext(TabContext)
	if(!context) {
		throw new Error('Tab compound component cannot be rendered outside the Tab component ')
	}
	return context
}

const TabsProvider = ({children}) => {
	const [ tabSelected, setTabSelected ] = useState(StepSelected.GENERAL)

	return (
		<TabContext.Provider value={{tabSelected, setTabSelected}}>
			{children}
		</TabContext.Provider>
	)

}

export { TabsProvider, useTabContext}
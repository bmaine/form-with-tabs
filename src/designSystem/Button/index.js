import React from 'react'
import styled from 'styled-components'
import { Button as MaterialUIButton } from '@material-ui/core'

const ButtonWrapper = styled(MaterialUIButton)`
    color: lightgrey;
    padding: 6px 12px;
    overflow: hidden;
    border-bottom: red;
`

const Button = ({children}) => {
	return <ButtonWrapper> {children} </ButtonWrapper>
}

export default Button
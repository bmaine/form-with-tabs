import React from 'react'
import styled from 'styled-components'
import { motion } from 'framer-motion'

const CircleLoaderWrapper = styled.div`
	position: relative;
	width: 3em;
	height: 3em;
`

const CircleLoaderAnimation = styled(motion.span)`
	display: block;
  width: 3em;
  height: 3em;
  border: 0.5em solid #E9E9E9;
  border-top: 0.5em solid #3498DB;
  border-radius: 50%;
  position: absolute;
  box-sizing: border-box;
  top: 0;
  left: 0;
`

const spinTransition = {
	loop: Infinity,
	duration: 1,
	ease: 'linear',
}

const CircleLoader = () =>
	<CircleLoaderWrapper>
		<CircleLoaderAnimation
			animate={{rotate: 360}}
			transition={spinTransition}

		/>


	</CircleLoaderWrapper>

export default CircleLoader
import React from 'react'
import styled from 'styled-components'

const TabWrapper =  styled.div`
max-height: 3vh;
	display: flex;
	flex-grow: 1;
	flex-direction: column;
    margin: 2vh 1vh;
    ${({isSelected}) => isSelected && `
    	border-bottom: 1px solid;
    `}
`
const Tab = ({isSelected,text}) => {
	return (
		<TabWrapper isSelected={isSelected}>
			{text}
		</TabWrapper>)
}

export default Tab
import {some} from 'lodash'

export const checkIfUserAlreadyExist = (users, value) => some(users, user => user.email === value?.email)

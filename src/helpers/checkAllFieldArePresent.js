import {isEmpty} from 'lodash'

const emailValidator = email => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)

export const checkAllFieldArePresent = user => {
	if (!user || isEmpty(user)) {
		return false
	}
	const {email, firstName, userName} = user
	const isEmailValid = email && emailValidator(email)
	return !!(isEmailValid && firstName && userName && email)
}

